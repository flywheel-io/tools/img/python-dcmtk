# flywheel/python-dcmtk

Python base image with [DCMTK](https://dicom.offis.de/en/dcmtk/).

## Tags

The following tagging scheme is published on [dockerhub](https://hub.docker.com/repository/docker/flywheel/python-dcmtk/tags?page=1&ordering=last_updated):

- `flywheel/python-dcmtk` - latest build from `main`
- `flywheel/python-dcmtk:d34db33f` - commit-specific build from `main`

## Usage

```dockerfile
FROM flywheel/python-dcmtk
```

## Building

```bash
docker build . -tflywheel/python-dcmtk
```

## Linting

```bash
pre-commit run -a
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
