FROM flywheel/python:3.13-alpine-build
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /src
COPY dcmtk.patch .
ARG V_DCMTK=3.6.8
RUN apk --no-cache add -tbuild-deps \
        build-base cmake libpng-dev libxml2-dev openjpeg-dev openssl-dev zlib-dev; \
    xh -IF https://github.com/DCMTK/dcmtk/archive/refs/tags/DCMTK-$V_DCMTK.tar.gz | tar xz; \
    patch -d"dcmtk-DCMTK-$V_DCMTK" -i../dcmtk.patch -p1; \
    cmake "dcmtk-DCMTK-$V_DCMTK" \
        -DBUILD_SHARED_LIBS=ON \
        -DCMAKE_C_FLAGS="-fPIC -D_LARGEFILE64_SOURCE" \
        -DCMAKE_CXX_FLAGS="-fPIC -D_LARGEFILE64_SOURCE" \
        -DDCMTK_DEFAULT_DICT=builtin \
        -DDCMTK_ENABLE_PRIVATE_TAGS=ON \
        -DDCMTK_ENABLE_STL=ON \
    ; \
    make install -j"$(nproc)"; \
    rm -rf /src/*; \
    apk del build-deps; \
    dcmdump --help
RUN /cleanup_build_deps.sh
USER flywheel
ONBUILD USER root
ONBUILD RUN apk --no-cache upgrade
